package me.taucu.bukkitutils.world;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.BoundingBox;

import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class WorldUtil {

    public static CuboidBlockIterator blocksIntersecting(BoundingBox box, World w) {
        return CuboidBlockIterator.of(w, box);
    }

    public static Stream<Block> streamBlocksIntersecting(BoundingBox box, World w) {
        return StreamSupport.stream(new Spliterator<Block>() {
            static final int CHARACTERISTICS = IMMUTABLE & NONNULL & SIZED & ORDERED & DISTINCT;
            CuboidBlockIterator it = blocksIntersecting(box, w);

            @Override
            public boolean tryAdvance(Consumer<? super Block> action) {
                if (it.hasNext()) {
                    action.accept(it.next());
                    return true;
                }
                return false;
            }

            @Override
            public Spliterator<Block> trySplit() {
                return null;
            }

            @Override
            public long estimateSize() {
                return it.remainingElements();
            }

            @Override
            public int characteristics() {
                return CHARACTERISTICS;
            }
        }, false);
    }

    public static boolean eachBlockIntersecting(BoundingBox box, World w, Predicate<Block> action) {
        CuboidBlockIterator it = CuboidBlockIterator.of(w, box);
        while (it.hasNext()) {
            if (!action.test(it.next())) {
                return false;
            }
        }
        return true;
    }

    public static void eachImpassibleBlockCollidingWith(BoundingBox box, World w, Predicate<Block> action) {
        eachBlockIntersecting(box, w, check -> {
            if (collidesWith(check, box)) {
                return action.test(check);
            }
            return true;
        });
    }

    /**
     * Checks if a blocks collision shapes' collides with an absolute bounding box
     *
     * @param check the block to check collision shapes with
     * @param box   the bounding box to check
     * @return true if the Box collides with any of the Blocks' collision shapes<br> false if there is no collision or if the block is {@link Block#isPassable()}
     */
    public static boolean collidesWith(Block check, BoundingBox box) {
        if (!check.isPassable()) {
            for (BoundingBox blockBox : check.getCollisionShape().getBoundingBoxes()) {
                blockBox.shift(check.getX(), check.getY(), check.getZ());
                if (box.overlaps(blockBox)) {
                    return true;
                }
            }
        }
        return false;
    }

}
