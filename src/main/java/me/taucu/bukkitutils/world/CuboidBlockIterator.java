package me.taucu.bukkitutils.world;

import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.NumberConversions;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class CuboidBlockIterator implements Iterator<Block> {

    final World world;
    final int blx, bly, blz;
    final int bgx, bgy, bgz;

    int x, y, z;

    Chunk chunk = null;
    long initialElements;
    long iterated;

    protected CuboidBlockIterator(World world, int blx, int bly, int blz, int bgx, int bgy, int bgz) {
        this.world = world;
        this.blx = blx;
        this.bly = bly;
        this.blz = blz;

        this.bgx = bgx;
        this.bgy = bgy;
        this.bgz = bgz;

        x = blx;
        y = bly;
        z = blz;

        initialElements = ((long) bgx - blx) * (bgy - bly) * (bgz - blz);
    }

    @Override
    public boolean hasNext() {
        return z < bgz;
    }

    @Override
    public Block next() {
        if (z >= bgz) {
            throw new NoSuchElementException("no more blocks");
        }

        if (chunk == null || (chunk.getX() != x >> 4 || chunk.getZ() != z >> 4)) {
            chunk = world.getChunkAt(x >> 4, z >> 4);
        }

        Block block = chunk.getBlock(x & 15, y, z & 15);

        if (++x >= bgx) {
            x = blx;
            if (++y >= bgy) {
                y = bly;
                ++z;
            }
        }

        iterated++;
        return block;
    }

    public long initialElements() {
        return initialElements;
    }

    public long remainingElements() {
        return initialElements - iterated;
    }

    public static CuboidBlockIterator of(World world, int x1, int y1, int z1, int x2, int y2, int z2) {
        return new CuboidBlockIterator(
                world,
                Math.min(x1, x2),
                Math.min(y1, y2),
                Math.min(z1, z2),
                Math.max(x1, x2),
                Math.max(y1, y2),
                Math.max(z1, z2)
        );
    }

    public static CuboidBlockIterator of(World world, double x1, double y1, double z1, double x2, double y2, double z2) {
        return new CuboidBlockIterator(
                world,
                NumberConversions.floor(Math.min(x1, x2)),
                NumberConversions.floor(Math.min(y1, y2)),
                NumberConversions.floor(Math.min(z1, z2)),
                NumberConversions.ceil(Math.max(x1, x2)),
                NumberConversions.ceil(Math.max(y1, y2)),
                NumberConversions.ceil(Math.max(z1, z2))
        );
    }

    public static CuboidBlockIterator of(World world, BoundingBox box) {
        int blx = NumberConversions.floor(box.getMinX());
        int bly = NumberConversions.floor(box.getMinY());
        int blz = NumberConversions.floor(box.getMinZ());

        int bgx = NumberConversions.ceil(box.getMaxX());
        int bgy = NumberConversions.ceil(box.getMaxY());
        int bgz = NumberConversions.ceil(box.getMaxZ());
        return new CuboidBlockIterator(world, blx, bly, blz, bgx, bgy, bgz);
    }

}
