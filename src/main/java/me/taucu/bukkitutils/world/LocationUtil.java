package me.taucu.bukkitutils.world;

import org.bukkit.Location;

public class LocationUtil {

    public static Location toCenterLocation(Location loc) {
        return new Location(loc.getWorld(), loc.getBlockX() + 0.5D, loc.getBlockY() + 0.5D, loc.getBlockZ() + 0.5D);
    }

}
