package me.taucu.bukkitutils.text;

import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;

import java.util.ArrayList;
import java.util.List;

public class Color {

    public static String convertFromChatColor(String str) {
        StrBuilder sb = new StrBuilder(str);
        for (int i = 0; i < sb.length() - 1; i++) {
            if (sb.charAt(i) == ChatColor.COLOR_CHAR) {
                if (sb.length() - (i + 13) > 0 && sb.charAt(i + 1) == 'x') {
                    String hex = StringUtils.replaceOnce(StringUtils.replace(sb.substring(i, i + 14), ChatColor.COLOR_CHAR + "", ""), "x", "#");
                    sb.delete(i, i + 14);
                    sb.insert(i, hex);
                    i += 6;
                } else if (ChatColor.getByChar(sb.charAt(i + 1)) != null) {
                    sb.setCharAt(i, '&');
                    sb.setCharAt(i + 1, Character.toLowerCase(sb.charAt(i + 1)));
                } else {
                    sb.setCharAt(i, '&');
                }
            }
        }
        return sb.toString();
    }

    public static String convertToChatColor(String str) {
        StrBuilder sb = new StrBuilder(str);
        for (int i = 0; i < sb.length() - 1; i++) {
            if (sb.charAt(i) == '&' && ChatColor.ALL_CODES.indexOf(sb.charAt(i + 1)) > -1) {
                sb.setCharAt(i, ChatColor.COLOR_CHAR);
                sb.setCharAt(i + 1, Character.toLowerCase(sb.charAt(i + 1)));
            } else if (sb.charAt(i) == '#' && sb.length() - (i + 6) > 0) {
                try {
                    String hex = ChatColor.of(sb.substring(i, i + 7)).toString();
                    sb.delete(i, i + 7);
                    sb.insert(i, hex);
                } catch (IllegalArgumentException ignored) {
                }
            }
        }
        return sb.toString();
    }

    public static ChatColor getLastColorOf(String str) {
        List<ChatColor> cl = getUsedColors(str);
        if (cl.size() == 0) {
            return ChatColor.RESET;
        }
        return cl.get(cl.size() - 1);
    }

    public static List<ChatColor> getUsedUnconvertedColors(String str) {
        List<ChatColor> colorList = new ArrayList<ChatColor>();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length - 1; i++) {
            switch (chars[i]) {
                case '&':
                    ChatColor color = ChatColor.getByChar(chars[i + 1]);
                    if (color != null) {
                        colorList.add(color);
                        break;
                    }
                case '#':
                    if (chars.length - (i + 6) > 0) {
                        try {
                            ChatColor color2 = ChatColor.of(new String(new char[]{chars[i], chars[i + 1], chars[i + 2], chars[i + 3], chars[i + 4], chars[i + 5], chars[i + 6]}));
                            if (color2 != null) {
                                colorList.add(color2);
                            }
                            i += 6;
                        } catch (IllegalArgumentException ignored) {
                        }
                    }
                default:
                    break;
            }
        }
        return colorList;
    }

    public static List<ChatColor> getUsedColors(String str) {
        List<ChatColor> colorList = new ArrayList<ChatColor>();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length - 1; i++) {

            if (chars[i] == ChatColor.COLOR_CHAR) {
                ChatColor color = ChatColor.getByChar(chars[i + 1]);
                if (color != null) {
                    colorList.add(color);
                } else if (chars.length - (i + 13) > 0 && chars[i + 1] == 'x') {
                    String hex = new String(new char[]{'#', chars[i + 3], chars[i + 5], chars[i + 7], chars[i + 9], chars[i + 11], chars[i + 13]});
                    try {
                        color = ChatColor.of(hex);
                        if (color != null) {
                            colorList.add(color);
                            i += 13;
                        }
                    } catch (IllegalArgumentException ignored) {
                    }
                }
            }
        }
        return colorList;
    }

    public static String toString(ChatColor c) {
        if (c == null) {
            return ChatColor.RESET + "";
        } else {
            return c + "";
        }
    }

}
