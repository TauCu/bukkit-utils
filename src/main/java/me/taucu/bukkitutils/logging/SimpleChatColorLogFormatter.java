package me.taucu.bukkitutils.logging;

import me.taucu.tauutils.logging.SimpleLogFormatter;
import net.md_5.bungee.api.ChatColor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class SimpleChatColorLogFormatter extends SimpleLogFormatter {

    public static final SimpleLogFormatter CHATCOLOR_INSTANCE_NEWLINE = new SimpleChatColorLogFormatter();
    public static final SimpleLogFormatter CHATCOLOR_INSTANCE = new SimpleChatColorLogFormatter();

    private final ChatColor defaultColor, warnColor, severeColor;

    public SimpleChatColorLogFormatter() {
        this(ChatColor.GRAY, ChatColor.YELLOW, ChatColor.RED);
    }

    public SimpleChatColorLogFormatter(ChatColor defaultColor, ChatColor warnColor, ChatColor severeColor) {
        this(false, defaultColor, warnColor, severeColor);
    }

    public SimpleChatColorLogFormatter(boolean newline, ChatColor defaultColor, ChatColor warnColor, ChatColor severeColor) {
        super(newline);
        this.defaultColor = defaultColor;
        this.warnColor = warnColor;
        this.severeColor = severeColor;
    }

    @Override
    public String format(LogRecord record) {
        Level level = record.getLevel();
        ChatColor color;
        switch (level.intValue()) {
            case 900:
                color = warnColor;
                break;
            case 1000:
                color = severeColor;
                break;
            default:
                color = defaultColor;
                break;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(color).append("[");
        sb.append(record.getLevel().getLocalizedName());
        sb.append("]: [");
        sb.append(record.getLoggerName());
        sb.append("] ");
        sb.append(record.getMessage());
        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            pw.print('\n');
            record.getThrown().printStackTrace(pw);
            pw.close();
            sb.append(sw.toString().replaceAll("[^\\x20-\\x7E\\n]+", " "));
        }
        if (newline) {
            sb.append('\n');
        }
        return sb.toString();
    }
}
