package me.taucu.bukkitutils.logging;

import me.taucu.tauutils.logging.LogUtils;
import org.bukkit.command.CommandSender;

import java.util.function.Consumer;
import java.util.logging.Formatter;
import java.util.logging.Logger;

public class BukkitLogUtils extends LogUtils {
    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * <p>
     * The handler will use the {@link SimpleChatColorLogFormatter#CHATCOLOR_INSTANCE} as its formatter
     * @param log the {@link Logger} to attach to
     * @param sender the {@link CommandSender} to forward logs to via {@link CommandSender#sendMessage(String)}
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachLogger(Logger log, CommandSender sender) {
        return attachToLogger(log, sender::sendMessage);
    }

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * @param log the {@link Logger} to attach to
     * @param formatter the {@link Formatter} to use
     * @param sender the {@link CommandSender} to forward logs to via {@link CommandSender#sendMessage(String)}
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachLogger(Logger log, Formatter formatter, CommandSender sender) {
        return attachToLogger(log, formatter, sender::sendMessage);
    }

    /**
     * Creates an {@link AutoCloseable} wrapper for a handler and attaches it to this {@link Logger}
     * <p>
     * The handler will use the {@link SimpleChatColorLogFormatter#CHATCOLOR_INSTANCE} as its formatter
     * @param log the {@link Logger} to attach to
     * @param consumer the {@link Consumer} to forward logs to
     * @return the {@link LoggerAttachment}
     */
    public static LoggerAttachment attachToLogger(Logger log, Consumer<String> consumer) {
        return attachToLogger(log, SimpleChatColorLogFormatter.CHATCOLOR_INSTANCE, consumer);
    }
}
