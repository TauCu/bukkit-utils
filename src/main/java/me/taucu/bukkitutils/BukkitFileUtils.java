package me.taucu.bukkitutils;

import me.taucu.tauutils.FileUtils;

import java.text.DecimalFormat;

public class BukkitFileUtils extends FileUtils {

    public static String formatFilesize(double d, DecimalFormat df, Object chatColor) {
        return formatFilesize(d, df, b -> {
            if (b == -1) {
                return chatColor + "B";
            } else {
                return chatColor + "" + FileUtils.FILE_SIZE_MAGNITUDES[b] + "B";
            }
        });
    }

}
