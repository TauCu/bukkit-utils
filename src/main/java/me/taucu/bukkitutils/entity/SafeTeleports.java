package me.taucu.bukkitutils.entity;

import me.taucu.bukkitutils.world.LocationUtil;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.NumberConversions;
import org.bukkit.util.Vector;

public class SafeTeleports {

    public static boolean teleportToGround(Entity p, int maxRadius) {
        return teleportToGround(p, p.getLocation(), maxRadius);
    }

    public static boolean teleportToGround(Entity p, Location from, int maxRadius) {
        Location loc = findNearestSafeLocation(from, p.getBoundingBox(), maxRadius);
        if (loc != null) {
            loc.setPitch(from.getPitch());
            loc.setYaw(from.getYaw());
            p.teleport(loc);
            return true;
        }
        return false;
    }

    public static Location findNearestSafeLocation(Location from, BoundingBox box, int maxRadius) {
        World w = from.getWorld();
        Block block = from.getBlock();

        BoundingBox relBox = box.clone();
        relBox.shift(-relBox.getCenterX(), -relBox.getMinY(), -relBox.getCenterZ());
        relBox.shift(from.toVector());
        if (solidFloor(relBox, w) && !intersectsBad(relBox, w)) {
            return from;
        }

        int maxHeight = w.getMaxHeight();
        int minHeight = w.getMinHeight();

        int by = block.getY();
        int bx = block.getX();
        int bz = block.getZ();

        Block checkBlock = null;

        int py = by, ny = by;
        ny--;

        do {
            if (ny >= minHeight) {
                checkBlock = scanXZ(w, maxRadius, bx, ny, bz);
                ny--;
            }
            if (py <= maxHeight && checkBlock == null) {
                checkBlock = scanXZ(w, maxRadius, bx, py, bz);
                py++;
            }
        } while ((py <= maxHeight || ny >= minHeight) && checkBlock == null);

        if (checkBlock != null) {
            return resolveTopBlockTpLocation(checkBlock, box);
        }
        return null;
    }

    public static Block scanXZ(World w, int maxRadius, int bx, int by, int bz) {
        Block checkBlock = null;
        for (int radius = 1; radius < maxRadius && checkBlock == null; radius++) {
            for (int i = -radius; i <= radius && checkBlock == null; i++) {
                checkBlock = isGoodOrNull(w.getBlockAt(bx - radius, by, bz + i));
            }
            for (int i = -radius; i <= radius && checkBlock == null; i++) {
                checkBlock = isGoodOrNull(w.getBlockAt(bx + radius, by, bz - i));
            }
            for (int i = -radius; i <= radius && checkBlock == null; i++) {
                checkBlock = isGoodOrNull(w.getBlockAt(bx + i, by, bz - radius));
            }
            for (int i = -radius; i <= radius && checkBlock == null; i++) {
                checkBlock = isGoodOrNull(w.getBlockAt(bx - i, by, bz + radius));
            }
        }
        return checkBlock;
    }

    public static Block isGoodOrNull(Block check) {
        return isBad(check) ? null : check;
    }

    public static boolean solidFloor(BoundingBox box, World w) {
        int blx = NumberConversions.floor(box.getMinX()),
                bgx = NumberConversions.ceil(box.getMaxX()),
                blz = NumberConversions.floor(box.getMinZ()),
                bgz = NumberConversions.ceil(box.getMaxZ());
        int y = NumberConversions.floor(box.getMinY()) - 1;
        for (int x = blx; x < bgx; x++) {
            for (int z = blz; z < bgz; z++) {
                Block check = w.getBlockAt(x, y, z);
                if (!check.isPassable()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean intersectsBad(BoundingBox box, World w) {
        int blx = NumberConversions.floor(box.getMinX()),
                bgx = NumberConversions.ceil(box.getMaxX()),
                bly = NumberConversions.floor(box.getMinY()),
                bgy = NumberConversions.ceil(box.getMaxY()),
                blz = NumberConversions.floor(box.getMinZ()),
                bgz = NumberConversions.ceil(box.getMaxZ());
        for (int x = blx; x < bgx; x++) {
            for (int y = bly; y < bgy; y++) {
                for (int z = blz; z < bgz; z++) {
                    Block check = w.getBlockAt(x, y, z);
                    if (badMaterialSwitch(check.getType())) {
                        return true;
                    } else if (!check.isPassable()) {
                        for (BoundingBox blockBox : check.getCollisionShape().getBoundingBoxes()) {
                            // very gay CHOOSE ONE OR THE OTHER! RELATIVE OR NOT RELATIVE FOR FUCKS SAKE
                            blockBox.shift(check.getLocation());
                            if (box.overlaps(blockBox)) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static Block checkForSafeBlockDown(Block block) {
        World world = block.getWorld();
        int minHeight = world.getMinHeight();

        if (block.getY() > world.getMaxHeight()) { // if the block is above the build limit set the starting block to build limit+1
            block = block.getRelative(0, world.getMaxHeight() + 1, 0);
        }
        while (block.isPassable() || isBadAbove(block)) {
            block = block.getRelative(BlockFace.DOWN);
            if (block.getY() < minHeight) {
                return null;
            }
        }
        return block;
    }

    public static boolean isBad(Block block) {
        Material type = block.getType();
        return (block.isPassable() && !(type == Material.WATER && isGoodWaterBelow(block))) || badMaterialSwitch(type) || isBadAbove(block);
    }

    public static boolean badMaterialSwitch(Material type) {
        switch (type) {
            case LAVA:
            case MAGMA_BLOCK:
            case FIRE:
            case SOUL_FIRE:
            case CAMPFIRE:
            case SOUL_CAMPFIRE:
            case WITHER_ROSE:
                return true;
            default:
                return false;
        }
    }

    public static boolean isGoodWaterBelow(Block block) {
        for (int i = 0; i < 4; i++) {
            block = block.getRelative(BlockFace.DOWN);
            if (block.getType() != Material.WATER && isBad(block)) {
                return false;
            }
        }
        return true;
    }

    /**
     * checks if the blocks above a player are "bad"
     *
     * @param block the block to check
     * @return true if bad false otherwise
     */
    public static boolean isBadAbove(Block block) {
        BoundingBox box = getHighestBoundingBoxOf(block);
        int start,
                stop;
        if (box == null) {
            start = 1;
            stop = 3;
        } else {
            start = Math.max(1, NumberConversions.ceil(box.getMaxY()));
            stop = 2 + start;
        }
        for (int i = start; i < stop; i++) {
            Block check = block.getRelative(0, i, 0);
            if (badMaterialSwitch(check.getType()) || !check.isPassable()) {
                return true;
            }
        }
        return false;
    }

    /**
     * does magic to return a location on this block to where you can teleport a player safely
     *
     * @param b   the block to resolve
     * @param box the players bounding box
     * @return an exact location to teleport the player
     */
    public static Location resolveTopBlockTpLocation(Block b, BoundingBox box) {
        box = box.clone();
        // gay
        box.shift(-box.getCenterX(), -box.getCenterY(), -box.getCenterZ());
        BoundingBox bbox = getHighestBoundingBoxOf(b);
        if (bbox == null) {
            return LocationUtil.toCenterLocation(b.getLocation()).add(0, 0.5, 0);
        }
        Vector center = bbox.getCenter();

        Location loc;
        if (center == null) {
            loc = LocationUtil.toCenterLocation(b.getLocation()).add(0, 1, 0);
        } else {
            center.setY(bbox.getMaxY());
            // shift location to accommodate player hitbox
            if (center.getX() + box.getMaxX() > 1) {
                center.setX(1 - box.getMaxX());
            } else if (center.getX() + box.getMinX() < 0) {
                center.setX(0 - box.getMinX());
            }

            if (center.getZ() + box.getMaxZ() > 1) {
                center.setZ(1 - box.getMaxZ());
            } else if (center.getZ() + box.getMinZ() < 0) {
                center.setZ(0 - box.getMinZ());
            }

            loc = center.toLocation(b.getWorld()).add(b.getX(), b.getY(), b.getZ());
        }

        return loc;
    }

    public static BoundingBox getHighestBoundingBoxOf(Block block) {
        double maxHeight = 0;
        BoundingBox highest = null;
        for (BoundingBox bbox : block.getCollisionShape().getBoundingBoxes()) {
            if (bbox.getMaxY() > maxHeight) {
                maxHeight = bbox.getMaxY();
                highest = bbox;
            }
        }
        return highest;
    }

}
