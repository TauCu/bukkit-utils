package me.taucu.bukkitutils.entity;

import org.bukkit.attribute.Attributable;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class DamageUtil {

    /**
     * Calculates the damage an {@link Entity} should take from the specified {@link DamageCause}
     * <br>
     * Applying any damage scalars such as Armor, Enchantments and Status Effects
     *
     * @param ent    the entity to damage
     * @param damage the damage to scale
     * @param cause  the cause of the damage (different causes scale differently)
     * @return the final damage with all outstanding scalars applied
     */
    public static double calculateDamageResistanceFor(Entity ent, double damage, DamageCause cause) {
        if (cause != DamageCause.MAGIC && cause != DamageCause.VOID && ent instanceof Attributable) {
            damage = calculateArmorDamageResistance((Attributable) ent, damage, cause);
        }
        if (ent instanceof LivingEntity) {
            damage = calculatePotionDamageResistance((LivingEntity) ent, damage, cause);
        }
        return damage;
    }

    /**
     * Calculates the damage an {@link Attributable} should take from the specified {@link DamageCause}
     * <br>
     * Applying any Armor damage scalars including Enchantments to the damage
     *
     * @param attributable the Attributable instance (such as a {@link LivingEntity})
     * @param damage       the damage to scale
     * @param cause        the cause of the damage (different causes scale differently)
     * @return the final damage with all outstanding scalars applied
     */
    public static double calculateArmorDamageResistance(Attributable attributable, double damage, DamageCause cause) {
        AttributeInstance genericArmor = attributable.getAttribute(Attribute.GENERIC_ARMOR);
        AttributeInstance genericArmorToughness = attributable.getAttribute(Attribute.GENERIC_ARMOR_TOUGHNESS);
        if (genericArmor != null && genericArmorToughness != null) {
            double armor = genericArmor.getValue();
            damage = damage * (1 - Math.min(20, Math.max(armor / 5, armor - damage / (2 + genericArmorToughness.getValue() / 4))) / 25);
            if (attributable instanceof LivingEntity) {
                damage = damage * (1 - (Math.min(20.0, getEffectiveProtectionLevelAgainst((LivingEntity) attributable, cause)) / 25));
            }
        }
        return damage;
    }

    /**
     * Calculates the damage a {@link LivingEntity} should take from the specified {@link DamageCause}
     * <br>
     * Applying any potion resistances to that damage
     *
     * @param ent    the entity to damage
     * @param damage the damage to scale
     * @param cause  the cause of the damage (different causes scale differently)
     * @return the final damage with any outstanding scalars applied
     */
    public static double calculatePotionDamageResistance(LivingEntity ent, double damage, DamageCause cause) {
        switch (cause) {
            case LAVA:
            case HOT_FLOOR:
            case FIRE:
            case FIRE_TICK:
                if (ent.getPotionEffect(PotionEffectType.FIRE_RESISTANCE) != null) {
                    return 0;
                }
            default:
                PotionEffect resistance = ent.getPotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
                if (resistance != null) {
                    damage = damage * (1D - ((resistance.getAmplifier() + 1) * 0.2D));
                }
            case STARVATION:
            case VOID:
                break;
        }
        return damage;
    }

    /**
     * Gets the {@link Enchantment} level of protection a {@link LivingEntity} has against the specified {@link DamageCause}
     *
     * @param ent   the entity to check
     * @param cause the damage cause
     * @return the level of protection or zero if the entity has no protection against that {@link DamageCause}
     */
    public static double getEffectiveProtectionLevelAgainst(LivingEntity ent, DamageCause cause) {
        double lvls = 0;
        for (ItemStack stack : ent.getEquipment().getArmorContents()) {
            lvls += getEffectiveProtectionLevelAgainst(stack, cause);
        }
        return lvls;
    }

    /**
     * Gets the {@link Enchantment} level of protection that an {@link ItemStack} has against the specified {@link DamageCause}
     *
     * @param stack the item to check
     * @param cause the damage cause
     * @return the level of protection or zero if the item has no protection against that {@link DamageCause}
     */
    public static double getEffectiveProtectionLevelAgainst(ItemStack stack, DamageCause cause) {
        double value = 0;
        if (stack != null && cause != DamageCause.MAGIC && cause != DamageCause.VOID) {
            switch (cause) {
                case BLOCK_EXPLOSION:
                case ENTITY_EXPLOSION:
                    value += stack.getEnchantmentLevel(Enchantment.PROTECTION_EXPLOSIONS);
                    break;
                case PROJECTILE:
                    value += stack.getEnchantmentLevel(Enchantment.PROTECTION_PROJECTILE);
                    break;
                case FIRE:
                case FIRE_TICK:
                case HOT_FLOOR:
                    value += stack.getEnchantmentLevel(Enchantment.PROTECTION_FIRE);
                    break;
                case FALL:
                    value += stack.getEnchantmentLevel(Enchantment.PROTECTION_FALL);
                    break;

                default:
                    break;
            }
            value += stack.getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL);
        }
        return value;
    }

}
