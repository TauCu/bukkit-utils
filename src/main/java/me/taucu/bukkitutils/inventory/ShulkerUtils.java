package me.taucu.bukkitutils.inventory;

import org.bukkit.block.BlockState;
import org.bukkit.block.ShulkerBox;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.function.Consumer;

public class ShulkerUtils {

    public static void doForEachIfShulker(ItemStack[] stacks, Consumer<BoxContainer> consumer) {
        for (ItemStack i : stacks) {
            doIfShulker(i, consumer);
        }
    }

    public static void doForEachIfShulker(Iterable<ItemStack> stacks, Consumer<BoxContainer> consumer) {
        for (ItemStack i : stacks) {
            doIfShulker(i, consumer);
        }
    }

    public static void doIfShulker(ItemStack i, Consumer<BoxContainer> consumer) {
        if (i != null && i.getItemMeta() instanceof BlockStateMeta meta) {
            BlockState state = meta.getBlockState();
            if (state instanceof ShulkerBox) {
                consumer.accept(new BoxContainer(i, meta, (ShulkerBox) state));
            }
        }
    }

    public static boolean isShulker(ItemStack i) {
        if (i != null) {
            ItemMeta meta = i.getItemMeta();
            return i != null && meta instanceof BlockStateMeta
                    && ((BlockStateMeta) meta).getBlockState() instanceof ShulkerBox;
        }
        return false;
    }

    public static class BoxContainer {
        private final ItemStack stack;
        private final BlockStateMeta meta;
        private final ShulkerBox box;

        public BoxContainer(ItemStack stack, BlockStateMeta meta, ShulkerBox box) {
            this.stack = stack;
            this.meta = meta;
            this.box = box;
        }

        public ShulkerBox get() {
            return box;
        }

        public BlockStateMeta getMeta() {
            return meta;
        }

        public ItemStack getStack() {
            return stack;
        }

        public void update() {
            box.update();
            meta.setBlockState(box);
            stack.setItemMeta(meta);
        }
    }

}
