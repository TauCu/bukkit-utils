package me.taucu.bukkitutils.commands;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.*;
import org.bukkit.plugin.Plugin;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.Map.Entry;

public class CommandUtil {

    public static final TabExecutor EMPTY_EXEC = new TabExecutor() {
        @Override
        public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
            return false;
        }

        @Override
        public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
            return null;
        }
    };

    public static boolean registerCommand(PluginCommand cmd) {
        CommandMap map = getCommandMap();
        return map.register(cmd.getPlugin().getDescription().getName(), cmd);
    }

    public static boolean forceRegisterCommand(PluginCommand cmd, boolean forceAlias) {
        return forceRegisterCommand(cmd, forceAlias, cmd.getPlugin().getDescription().getName(), true);
    }

    public static boolean forceRegisterCommand(Command cmd, boolean forceAlias, String fallbackPrefix, boolean forcePrefix) {
        CommandMap map = getCommandMap();
        List<String> toRemove = new ArrayList<String>();
        toRemove.add(cmd.getName());
        String check = fallbackPrefix + ":" + cmd.getName();
        if (forcePrefix && getCommand(check) != null) {
            toRemove.add(check);
        }
        if (forceAlias) {
            toRemove.addAll(cmd.getAliases());
        }
        unregisterCommandAsStrings(toRemove, false);
        return map.register(fallbackPrefix, cmd);
    }

    public static boolean unregisterCommandAsString(String cmd) {
        return unregisterCommandAsStrings(Arrays.asList(cmd), true);
    }

    public static boolean unregisterCommandAsStrings(Collection<String> cmds, boolean unregister) {
        boolean changed = false;
        CommandMap map = getCommandMap();
        Map<String, Command> knownCmds = getKnownCommands(map);
        HashMap<String, Command> knownCmdsCopy = new HashMap<String, Command>(knownCmds);

        HashSet<String> toRemove = new HashSet<String>();
        cmds.forEach(c -> toRemove.add(c.toLowerCase()));

        for (Entry<String, Command> check : knownCmdsCopy.entrySet()) {
            if (toRemove.contains(check.getKey().toLowerCase())) {
                changed = true;
                Command removed = knownCmds.remove(check.getKey());
                if (unregister && removed != null) {
                    removed.unregister(map);
                }
            }
        }

        return changed;
    }

    public static boolean unregisterCommand(Command cmd) {
        HashSet<Command> set = new HashSet<Command>(1);
        set.add(cmd);
        return unregisterCommands(set);
    }

    public static boolean unregisterCommands(Collection<? extends Command> cmds) {
        boolean changed = false;
        CommandMap commandMap = getCommandMap();
        Map<String, Command> knownCmds = getKnownCommands(commandMap);

        HashMap<String, Command> commandsToCheck = new HashMap<String, Command>();

        for (Command c : cmds) {
            commandsToCheck.put(c.getLabel().toLowerCase(), c);
            commandsToCheck.put(c.getName().toLowerCase(), c);
            c.getAliases().forEach(a -> commandsToCheck.put(a.toLowerCase(), c));
        }

        for (Entry<String, Command> check : commandsToCheck.entrySet()) {
            Command mappedCommand = knownCmds.get(check.getKey());
            if (check.getValue().equals(mappedCommand)) {
                mappedCommand.unregister(commandMap);
                knownCmds.remove(check.getKey());
                changed = true;
            } else if (check.getValue() instanceof PluginCommand checkPCmd) {
                if (mappedCommand instanceof PluginCommand mappedPCmd) {
                    CommandExecutor mappedExec = mappedPCmd.getExecutor();

                    if (mappedExec != null && mappedExec.equals(checkPCmd.getExecutor())) {
                        mappedPCmd.setExecutor(null);
                        mappedPCmd.setTabCompleter(null);
                    }
                }
                checkPCmd.setExecutor(EMPTY_EXEC);
                checkPCmd.setTabCompleter(EMPTY_EXEC);
            }
        }
        return changed;
    }

    public static Command getCommand(String cmd) {
        return getKnownCommands(getCommandMap()).get(cmd);
    }

    public static List<PluginCommand> getCommands(Plugin plugin) {
        return getKnownCommands(getCommandMap())
                .values().stream()
                .filter(c -> c instanceof PluginCommand pc && pc.getPlugin() == plugin)
                .map(c -> (PluginCommand) c)
                .toList();
    }

    public static CommandExecutor getExecutor(PluginCommand cmd) {
        return cmd.getExecutor();
    }

    public static PluginCommand createCommand(Plugin plugin, String cmd, String... aliases) {

        PluginCommand pc = createCommand(plugin, cmd);
        List<String> aliases2 = new ArrayList<String>();
        for (String a : aliases) {
            aliases2.add(a.toLowerCase());
        }
        pc.setAliases(aliases2);

        return pc;
    }

    public static PluginCommand createCommand(Plugin plugin, String cmd) {
        PluginCommand command = null;
        cmd = cmd.toLowerCase();

        ReflectiveOperationException lastException = null;
        for (Constructor<?> constructor : PluginCommand.class.getDeclaredConstructors()) {
            try {
                constructor.setAccessible(true);
                command = (PluginCommand) constructor.newInstance(cmd, plugin);
            } catch (ReflectiveOperationException e) {
                lastException = e;
            }
        }

        if (command == null) {
            throw new RuntimeException("Could not create PluginCommand", lastException);
        }

        return command;
    }

    public static CommandMap getCommandMap() {
        Server server = Bukkit.getServer();
        try {
            Method m = server.getClass().getDeclaredMethod("getCommandMap");
            m.setAccessible(true);
            return (CommandMap) m.invoke(Bukkit.getServer());
        } catch (Exception ignored) {
        }
        try {
            Field commandMapField = server.getClass().getDeclaredField("commandMap");
            commandMapField.setAccessible(true);
            return (CommandMap) commandMapField.get(server);
        } catch (Exception e) {
            throw new RuntimeException("Could not get commandMap", e);
        }
    }

    @SuppressWarnings("unchecked")
    public static Map<String, Command> getKnownCommands(CommandMap m) {
        try {
            Method me = m.getClass().getDeclaredMethod("getKnownCommands");
            me.setAccessible(true);
            return (Map<String, Command>) me.invoke(m);
        } catch (Exception ignored) {
        }
        try {
            Field knownCommandsField = m.getClass().getDeclaredField("knownCommands");
            knownCommandsField.setAccessible(true);
            return (Map<String, Command>) knownCommandsField.get(m);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Could not get knownCommands", e);
        }
    }

    public static void syncCommands() {
        try {
            Method syncCommandsMethod = Bukkit.getServer().getClass().getDeclaredMethod("syncCommands");
            syncCommandsMethod.setAccessible(true);
            syncCommandsMethod.invoke(Bukkit.getServer());
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Could not invoke syncCommands method", e);
        }
    }

    public static List<PermissionMapping> getPermissionsMatching(String perm, boolean partial) {
        perm = perm.toLowerCase();
        List<PermissionMapping> matches = new ArrayList<PermissionMapping>();
        Map<String, Command> knownCommands = getKnownCommands(getCommandMap());
        for (String key : knownCommands.keySet()) {
            Command cmd = knownCommands.get(key);
            if (cmd == null || cmd.getPermission() == null) {
                continue;
            }
            if (partial) {
                if (cmd.getPermission().toLowerCase().startsWith(perm)) {
                    matches.add(new PermissionMapping(cmd instanceof PluginCommand ? ((PluginCommand) cmd).getExecutor().getClass().getName() : "null", key, cmd.getPermission()));
                }
            } else {
                if (cmd.getPermission().equalsIgnoreCase(perm)) {
                    matches.add(new PermissionMapping(cmd instanceof PluginCommand ? ((PluginCommand) cmd).getExecutor().getClass().getName() : "null", key, cmd.getPermission()));
                }
            }
        }
        return matches;
    }

    public static class PermissionMapping {

        private final String executor;
        private final String command;
        private final String permission;

        protected PermissionMapping(String executor, String command, String permission) {
            if (executor == null) executor = "null";
            if (command == null) command = "null";
            if (permission == null) permission = "null";
            this.executor = executor;
            this.command = command;
            this.permission = permission;
        }

        public String getExecutor() {
            return executor;
        }

        public String getCommand() {
            return command;
        }

        public String getPermission() {
            return permission;
        }

    }

}
